FROM alpine:3.15 AS builder

# Reference:
#  https://curl.se/docs/http3.html
#  https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=curl-http3
#  https://github.com/curl/curl/blob/master/docs/HTTP3.md#quiche-version

RUN set -ex \
  && apk add git cargo cmake make g++ autoconf automake libtool pkgconfig \
       zlib-dev brotli-dev zstd-dev nghttp2-dev \
  \
  && git clone --depth 1 --branch 0.12.0 --recursive https://github.com/cloudflare/quiche \
  && cd quiche \
  && cargo build --package quiche --release --features ffi,pkg-config-meta,qlog \
  && mkdir quiche/deps/boringssl/src/lib \
  && ln -vnf $(find target/release -name libcrypto.a -o -name libssl.a) quiche/deps/boringssl/src/lib/ \
  && cd .. \
  \
  && git clone --depth=1 --branch curl-7_82_0 https://github.com/curl/curl \
  && cd curl \
  && autoreconf -fi \
  && ./configure \
      LDFLAGS="-Wl,-rpath,$PWD/../quiche/target/release" \
      --with-openssl=$PWD/../quiche/quiche/deps/boringssl/src \
      --with-quiche=$PWD/../quiche/target/release \
      --prefix=/usr \
      --mandir=/usr/share/man \
  && make \
  && mkdir /build \
  && make DESTDIR="/build" install \
  && make DESTDIR="/build" install -C scripts \
  && cd .. \
  \
  && install -Dm755 quiche/target/release/libquiche.so /build/usr/lib/libquiche.so


FROM alpine:3.15

RUN set -ex \
  && apk add libstdc++ nghttp2-libs brotli-libs zstd-libs bash

COPY --from=builder /build /

CMD ["/usr/bin/curl"]
