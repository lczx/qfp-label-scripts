#!/usr/bin/env python
import sys
from argparse import ArgumentParser


HOSTS_FILTER_WORDS = [ 'sex', 'porn', 'gay', 'fap', 'hentai', 'xnxx', 'xvideos', 'xxx' ]

argp = ArgumentParser()
argp.add_argument('-b', metavar='hosts_file', help='Hosts file to use as a filter')
argp.add_argument('-w', metavar='words_file', help='List of words to use as a filter')
argp.add_argument('in_file', help='The file to process')


def load_filter(filename):
    return [line.split(' ')[1] for line in load_words(filename)]


def load_words(filename):
    with open(filename, 'r') as f:
        words = []
        for l in f:
            line = l.strip()
            if line == '' or line.startswith('#'):
                continue
            words.append(line)
        return words


if __name__ == '__main__':
    args = argp.parse_args()

    filter_hosts = load_filter(args.b) if args.b is not None else []
    filter_words = load_words(args.w) if args.w is not None else []

    count_in = 0
    count_out = 0

    file_in = open(args.in_file, 'r')

    for l in file_in:
        line = l.strip()
        line_tocheck = line.split(' ')[0]

        if line_tocheck not in filter_hosts and not any(w in line for w in filter_words):
            sys.stdout.write(line + '\n')
            count_out += 1
        count_in += 1

    file_in.close()

    print(f'In hosts: {count_in}, out hosts: {count_out}', file=sys.stderr)

