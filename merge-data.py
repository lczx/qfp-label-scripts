#!/usr/bin/env python
import sys
from urllib.parse import urlparse

from tldextract import extract as tldext


def read_and_filter(fname, filter_fn):
    """Loads newline-separated data from a file and applies the given filter"""
    data = []
    with open(fname, 'r') as f:
        for l in f:
            if l.startswith('#'):
                continue
            elem = filter_fn(l)
            if elem is not None:
                data.append(elem)
        return data


def to_hosts_dict(data):
    """Converts { fname: [(host, host_data)] } mappings to { host: {fnames: host_data} }"""
    hdict = {}
    for label, lines_data in data.items():
        for host, host_data in lines_data:
            if host not in hdict:
                hdict[host] = {}
            hdict[host][label] = host_data
    return hdict


def load_hdict(files, line_filter, data_filter=None):
    """Loads multiple data files and generates a hosts-labels dictionary"""
    def mapfn(fname):
        line_data = read_and_filter(fname, line_filter)
        return (fname, data_filter(line_data) if data_filter is not None else line_data)

    data = dict(map(mapfn, files))
    hdict = to_hosts_dict(data)
    return data.keys(), hdict


def print_hdict(labels, hdict):
    """Displays a hosts-labels dictionary as a table"""
    max_host_len = max(map(len, hdict.keys()))
    max_label_len = max(map(len, labels))

    print_fmt = f'%{max_host_len+4}s' + f'%{max_label_len+4}s' * len(labels)

    print(print_fmt % ('', *labels,))

    def getsym(hdata, label):
        if label not in hdata:
            return ''
        if type(hdata[label]) == bool:
            return 'x'
        return len(hdata[label])

    for h, ls in hdict.items():
        strs = map(lambda l: getsym(ls, l), labels)
        print(print_fmt % (h, *strs))


def make_intersect(labels, hdict, with_host_data=False):
    """Returns all hosts that include all of the given labels"""
    result = []
    for host, ls in hdict.items():
        if all(l in ls for l in labels):
            if with_host_data:
                result.append((host, set.intersection(*ls.values())))
            else:
                result.append(host)
    return result


def filter_line_asvc(line):
    """Line filter for asvc mode - parses curl alt-svc cache files"""
    # https://curl.se/docs/alt-svc.html
    src_proto, src_host, src_port, dst_proto, dst_host, dst_port, expire_a, expire_b, persist, unused = \
        line.strip().split(' ')

    if src_host != dst_host or src_port != dst_port:
        print('Warn: "%s:%s" is not "%s:%s"' %
              (src_host, src_port, dst_host, dst_port), file=sys.stderr)
        return None

    if not dst_proto.startswith('h3'):
        print('Warn: "%s" alt-svc is not h3' % (dst_host), file=sys.stderr)
        return None

    return dst_host, True


def filter_line_redir(line):
    """Line filter for quic redirects mode - parses "request-bulk.sh qr" results"""
    host, err, code, httpv, size, n_redirs, *redirs = \
        line.strip().split(' ')

    # Ignore sites with errors or wrong HTTP version
    if err != '0' or int(code) >= 400 or httpv != '3':
        return None

    host_split = tldext(host)
    p_orig, *p_redirs = list(filter(lambda u: u.netloc !=
                             '', map(lambda u: urlparse(u), redirs)))

    # Ignore sites where all redirects have nothing in common with base host label
    if len(p_redirs) > 0 and all(tldext(r.netloc).domain != host_split.domain for r in p_redirs):
        return None

    # Ignore sites where any redirect has nonstandard port
    if len(p_redirs) > 0 and any(r.netloc.find(':') != -1 and not r.netloc.endswith(':443') for r in p_redirs):
        return None

    more_urls = map(
        lambda u: u._replace(netloc=u.netloc.removesuffix(':443')),
        filter(lambda u: (u.path == '/' or u.path == '')
               and u.query == '' and u.fragment == '', p_redirs)
    )

    urls = set([p_orig.geturl(), *map(lambda u: u.geturl(), more_urls)])

    return host, urls


def filter_unique(data):
    # filter_unique may be useless since grouping already filters out duplicates
    uniq = []
    for d in data:
        if d not in uniq:
            uniq.append(d)
    return uniq


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print(f'Usage: {sys.argv[0]} asvc|qr diff|out-intersect files...',
              file=sys.stderr)
        sys.exit(1)

    mode, op, *args = sys.argv[1:]

    def mode_filters():
        if mode == 'asvc':
            return (filter_line_asvc, None)
        if mode == 'qr':
            return (filter_line_redir, filter_unique)
        raise ValueError(f'Unknown mode "{mode}"')

    line_filter, data_filter = mode_filters()

    if op == 'diff':
        labels, hdict = load_hdict(args, line_filter, data_filter)
        print_hdict(labels, hdict)

    elif op == 'out-intersect':
        labels, hdict = load_hdict(args, line_filter, data_filter)

        if mode == 'asvc':
            out_fn = 'asvc-hosts-merged.txt'
            hosts = make_intersect(labels, hdict)
            with open(out_fn, 'w') as f:
                for line in hosts:
                    f.write(line + '\n')
                print(f'Written {len(hosts)} hosts to {out_fn}', file=sys.stderr)

        elif mode == 'qr':
            out_fn = 'qr-urls-merged.txt'
            hosts_urls = make_intersect(labels, hdict, True)
            with open(out_fn, 'w') as f:
                for h, urls in hosts_urls:
                    f.write(h + ' ' + ' '.join(urls) + '\n')
                print(f'Written {len(hosts_urls)} hosts to {out_fn}', file=sys.stderr)

    else:
        print(f'Unknown operation "{op}"', file=sys.stderr)
