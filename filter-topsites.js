#!/usr/bin/env node
const { createReadStream, createWriteStream } = require('fs');
const path = require('path');
const unzip = require('unzipper');
const csv = require('csv');
const tlds = require('tlds');
const cliProgress = require('cli-progress');


function readData(filename) {
  return new Promise((res, rej) => {
    createReadStream(filename)
      .pipe(unzip.Parse())
      .on('entry', entry =>
        entry.pipe(csv.parse({}, (err, data) => err == null ? res(data) : rej(err)))
      );
  });
}

function stripTld(domain) {
  const tld = tlds.find(t => domain.endsWith('.' + t));
  const newDom = tld == null ? domain : domain.slice(0, - tld.length - 1);

  if (newDom.endsWith('.co')) return newDom.slice(0, -3);
  if (newDom.endsWith('.com')) return newDom.slice(0, -4);
  return newDom;
}

function filterTlds(data, limit) {
  const inData = data.map(d => d[1]);

  const bar1 = new cliProgress.SingleBar({
    format: 'Stripping TLDs | {bar} | {percentage}% ({value}/{total} domains)',
    hideCursor: true
  }, cliProgress.Presets.shades_classic);
  bar1.start(limit, 0)

  const outPre = [];  // Would be nice to use sets but we want order

  let inIdx = 0;

  // Make a list of stripped TLDs
  while (outPre.length < limit && inIdx < data.length) {
    const domain = inData[inIdx];
    if (outPre.length % 100 === 0) bar1.update(outPre.length);

    const stripped = stripTld(domain);
    if (!outPre.includes(stripped)) outPre.push(stripped);

    inIdx++;
  }

  bar1.stop();

  const bar2 = new cliProgress.SingleBar({
    format: 'Rehidrating names | {bar} | {percentage}% ({value}/{total} domains)',
    hideCursor: true
  }, cliProgress.Presets.shades_classic);
  bar2.start(limit, 0)

  // Rehidrate list with TLDs
  const out = outPre.map((e, i) => {
    if (i % 100 === 0) bar2.update(i);
    return inData.find(d => d.startsWith(e + '.'))
  });

  bar2.stop();

  return out;
}

(async () => {
  if (process.argv.length < 5) {
    console.error(`Usage: ${path.basename(__filename)} site_count in_file out_file`);
    process.exit(1);
  }

  const [_node, _script, siteCountStr, inFile, outFile, ..._more] = process.argv;
  const siteCount = parseInt(siteCountStr);

  const data = await readData(inFile);
  console.log(`Read ${data.length} entries`);

  const filtered = filterTlds(data, siteCount);

  const f = createWriteStream(outFile);
  filtered.forEach(l => f.write(`${l}\n`));
  f.end();
  console.log(`Output written to "${outFile}"`);
})();
