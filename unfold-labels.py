#!/usr/bin/env python
import os
import sys


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(f'Usage: {sys.argv[0]} in_file', file=sys.stderr)
        sys.exit(1)

    in_fn = sys.argv[1];
    fname_base = os.path.splitext(in_fn)[0]
    out_fn = fname_base + '.rpurls'

    host_count = 0
    url_count = 0

    outf = open(out_fn, 'w')
    with open(in_fn, 'r') as f:
        for l in f:
            label, *urls = l.strip().split(' ')
            for u in urls:
                outf.write('%s {"label": "%s"}\n' % (u, label))
                url_count += 1
            host_count += 1
    outf.close()

    print(f'Written {url_count} urls for {host_count} hosts to {out_fn}', file=sys.stderr)
