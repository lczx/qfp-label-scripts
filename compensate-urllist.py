#!/usr/bin/env python
import os
import sys
from itertools import combinations, product
import json


def load_data(fname):
    data = {}
    with open(fname) as f:
        for line in f:
            url, meta = line.strip().split(' ', 1)
            label = json.loads(meta)['label']

            if label not in data:
                data[label] = []
            data[label].append(url)
    return data


def compute_mappings(counts):
    maps = []
    for c in counts:
        to_fill = ampl - c
        if to_fill % c == 0:
            maps.append([ list(range(c)) * to_fill ])
        elif to_fill > c:
            raise RuntimeError(f'Cannot fill {to_fill} with {c} entries')
        else:
            maps.append(list(map(list, combinations(range(c), to_fill))))
    return maps


def gen_variant(data, var, ampl):
    for label, ents in data.items():
        if len(ents) == ampl:
            continue

        mapping = var[len(ents)]
        new_ents = [ents[i] for i in mapping]

        meta = json.dumps({'label': label})
        for e in new_ents:
            yield f'{e} {meta}'


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(f'Usage: {os.path.basename(__file__)} in_rpurls', file=sys.stderr)
        sys.exit(1)

    in_file = sys.argv[1]

    # Load entities and group by label
    data = load_data(in_file)

    # Make a set of the possible nr. of entries for each label, its
    # maximum is the number of entries other labels need to be amplified to
    counts = set(len(v) for v in data.values())
    ampl = max(counts)
    counts.remove(ampl)

    # Compute the mapping from input entry indices to their amplification.
    # For each input length, we have an array of permutations,
    # each permutation is in turn an array of indices to map to.
    maps = compute_mappings(counts)

    # Make the product of input length permutations
    # to get a list of possible amplification variants
    variants = []
    for perm in product(*maps):
        variants.append({ampl - len(p): p for p in perm})

    print(f'Amplifying to {ampl} ({len(variants)} variants)', file=sys.stderr)

    for i, var in enumerate(variants):
        fn, fe = os.path.splitext(in_file)
        out_file = f'{fn}-a{i}{fe}'
        print(f'Variant {var} to "{out_file}"', file=sys.stderr)

        with open(out_file, 'w', encoding='utf8') as f:
            f.writelines(l + '\n' for l in gen_variant(data, var, ampl))
