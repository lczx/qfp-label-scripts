#!/bin/bash

MODE="$1"
IN_FILE="$2"
OUT_FILE="$3"
HEADERS_FILE=/tmp/_curl_headers

prompt() {
    while true; do
        read -rp "$1 (y/n) " yn
        case $yn in
            [yY] ) return 0;;
            [nN] ) return 1;;
        esac
    done
}

loadargs_firefox() {
    browser_args=(
        -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' \
        -H 'Accept-Language: en-US,en;q=0.5' \
        -H 'Accept-Encoding: gzip, deflate, br' \
        -H 'DNT: 1' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'Connection: keep-alive' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-Site: none' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-GPC: 1'
    )
}

loadargs_chrome() {
    browser_args=(
        -H "authority: $1" \
        -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'accept-language: en-US,en;q=0.9' \
        -H 'dnt: 1' \
        -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="100"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'sec-ch-ua-platform: "Linux"' \
        -H 'sec-fetch-dest: document' \
        -H 'sec-fetch-mode: navigate' \
        -H 'sec-fetch-site: none' \
        -H 'sec-fetch-user: ?1' \
        -H 'upgrade-insecure-requests: 1' \
        -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36' \
        --compressed
    )
}

tracereqs() {
    while read -r line; do
        if [[ "$line" == location:* ]]; then
            loc="$(echo "$line" | cut -d ' ' -f 2 )"
            locs+=( "${loc//[$'\t\r\n ']}" )
        fi
    done < "$HEADERS_FILE"
}

if [ "$MODE" != "asvc" ] && [ "$MODE" != "qr" ] || [ -z "$IN_FILE" ]; then
    echo "Usage: $(basename "$0") asvc|qr in_file [out_file]"
    exit 1
fi

if [ ! -f "$IN_FILE" ]; then
    echo "Input file \"$IN_FILE\" does not exists"
    exit 1
fi

if [ "$MODE" == "qr" ] && ! curl -V | grep -q HTTP3 ; then
    echo "The installed curl does not support HTTP/3!"
    exit 1
fi

# If no out file was given, generate one using mode, hostname and the current date
if [ -z "$OUT_FILE" ]; then
    OUT_FILE=$MODE-${HOSTNAME::3}-$(date +%b%d | tr '[:upper:]' '[:lower:]')
fi

LOG_FILE=$OUT_FILE.log
STATE_FILE=$OUT_FILE.state

if [ -f "$STATE_FILE" ]; then
    curr=$(cat "$STATE_FILE")
    rm "$STATE_FILE"

    # Remove last pending line from outpit in redirects mode
    [ "$MODE" == "qr" ] && sed -i '$ d' "$OUT_FILE"

    echo Session resumed | tee -a "$LOG_FILE"

else
    if [ -f "$OUT_FILE" ]; then
        prompt 'Output file already exists, remove it?' \
            && rm "$OUT_FILE" \
            || exit 1
    fi
    curr=1
    touch "$OUT_FILE"
    rm -f "$LOG_FILE"
fi

lines=$(wc -l < "$IN_FILE")

tail -n +$curr "$IN_FILE" | while read -r domain; do
    trap 'echo "$curr" > $STATE_FILE; echo Session halted; exit' INT

    "loadargs_${CURL_BROWSER:-chrome}" "$domain"

    if [ "$MODE" == "asvc" ]; then
        h3_sites=$(tail -n +3 "$OUT_FILE" | cut -d ' ' -f 4 | grep -c h3)
        echo "~ $curr/$lines ($h3_sites) $domain" | tee -a "$LOG_FILE"

        curl -fsS -m5 --alt-svc "$OUT_FILE" "${browser_args[@]}" \
            "https://$domain/" \
             1> /dev/null 2> >(tee -a "$LOG_FILE" >&2)

    elif [ "$MODE" == "qr" ]; then
        echo "~ $curr/$lines $domain" | tee -a "$LOG_FILE"

        echo -n "$domain " >> "$OUT_FILE"

        curlret=$(
            curl --http3 -fsL -m5 -D "$HEADERS_FILE" -o /dev/null "${browser_args[@]}" \
                -w '%{exitcode} %{http_code} %{http_version} %{size_download} %{num_redirects}' \
                "https://$domain/"
        )
        locs=( "https://$domain/" )
        [ "${PIPESTATUS[0]}" == '0' ] && tracereqs

        echo "$curlret" "${locs[@]}" >> "$OUT_FILE"
    fi

    curr=$((curr + 1))
done
