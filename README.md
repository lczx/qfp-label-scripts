# QFP Label Scripts

_A collection of scripts made to collect and filter hostnames to be used as
labels in my website fingerprinting research attempts..._

## How to use

Start by fetching Node (`yarn install`) and Python (`pip install -r
requirements.txt`) dependencies, then download required datasets:

- `download-alexa1m.sh` fetches the _Alexa Top 1M sites_ list, the start of our
  pipeline, resulting in the `top-1m.csv.zip` file;

- `download-hosts-blacklist.sh` downloads [Steven Black's unified hosts
  blacklist](https://github.com/StevenBlack/hosts) as `hosts-blacklist`, used
  to filter out websites known for fake news, gambling and pornography.

Scripts in this repository require curl compiled with HTTP/3 support, a
Dockerfile is provided in the `curl-http3` folder which can be used to build an
image and run the scripts in a controlled environment satisfying this
requirement.

To build and run the image in temporary container, do:

```sh
cd curl-http3

# Using Docker
docker build -t curl3 .
docker run --rm -it -v $PWD:/data curl3 /bin/ash

# Using Podman (with SELinux)
podman build -t curl3 .
podman run --rm -it -v $PWD:/data --security-opt label=disable localhost/curl3 /bin/ash
```

### 1. Initial TLD filtering

```sh
# Usage: ./filter_topsites.js site_count in_file out_file
./filter-topsites.js 10000 top-1m.csv.zip topsites_10k_dedup.txt
```

The first step extracts the `top-1m.csv.zip` in memory and takes only the most
popular TLD for each domain (e.g. `.com` instead of `.co.uk`). Only the first
10k sites of the 1M list are considered and saved in `topsites_10k_dedup.txt`.

### 2. Fetch `Alt-Svc` for domains

```sh
# Usage ./request-bulk.sh asvc|qr in_file [out_file]
./request_bulk.sh asvc topsites_10k_dedup.txt
```

A regular HTTPS connection is made using `curl` to the obtained websites
(emulating headers used by a regular browser) to collect data for the response's
`Alt-Svc` header field.

Connecting to 10k sites can take quite a while. This scripts also outputs a log
file and makes use of a state file to allow resumption if interrupted.

> The `CURL_BROWSER` environment variable can be set to `firefox` or `chrome` to
> send headers like the selected browser would do.

### 3. Filter and merge `Alt-Svc` results

```sh
# Usage: ./merge-data.py asvc|qr diff|out-intersect files...
./merge-data.py asvc out-intersect asvc-*[^.log]
```

Given as input `Alt-Svc` databases collected from different sources at different
times, this script filters sites indicating support for HTTP/3.

The result can either be displayed as a table comparing the sources (`diff`
operation) or outputted as the intersection (i.e. domains seen in all captures)
as a list in `asvc-hosts-merged.txt` (`out-intersect` operation).

### 4. Filter using a blacklist

```sh
# Usage ./filter-hosts.py [-b hosts_file] [-w words_file] in_file > out_file
./filter-hosts.py -b hosts-blacklist -w words-blacklist \
    asvc-hosts-merged.txt > asvc-hosts-merged-filtered.txt
```

This step filters the previously merged domains list using the blacklist
in `hosts-filter` obtained by `download-hosts-filter.sh`. Output is written to
`asvc-hosts-merged-filtered.txt`.

### 5. Collect redirects data

```sh
# Usage ./request-bulk.sh asvc|qr in_file [out_file]
./request-bulk.sh qr asvc-hosts-merged-filtered.txt
```

An HTTP/3 connection is made to each host in the list, collecting status code
and all the performed intermediate redirects.

### 6. Filter and merge redirection results

```sh
# Usage: ./merge-data.py asvc|qr diff|out-intersect files...
./merge-data.py qr out-intersect qr-*[^.log]
```

Results from the last step collected from different sources go through a final
filtering step to consider only redirects with URLs to the same primary domain
without query and path data.

Only hosts that appear in all sources are considered and an intersection between
their redirect URLs is saved to `qr-urls-merged.txt`. These may be used to
increase variability of collected data for the same host label.

### 7. Convert to Reaper format

```sh
# (Optional) Filter out unwanted hosts
./filter-hosts.py -w out-blacklist qr-urls-merged.txt > qr-urls-merged-filtered.txt

# (Optional) Filter only the first 100 hosts
head -n100 qr-urls-merged-filtered.txt > qr100.txt

# Usage: ./unfold-labels.py in_file
./unfold-labels.py qr100.txt
```

Unfold the host URLs file to Reaper's URLs with metadata format.
[Reaper](https://gitlab.com/lczx/qfp-reaper) is an automated multi-browser
capture collector developed along with this project.

### 8. Compensate uneven entries per label

```sh
# Usage: ./compensate-urllist.py in_rpurls
./compensate-urllist.py qr100.rpurls
```

From the generated URL list, create complimentary lists such that, when
joined with the original list, all labels have the same amount of URLs.
